# zqAuthor - My Personal Website Admin Tool

Admin tools for my personal website, built with git & python & markdown

Check [www.qianzuncheng.com](http://www.qianzuncheng.com) for a demo

**Make sure to check [this repo](https://gitlab.oit.duke.edu/zq29/zqsite) first for an overview!**

## How the Admin Tool Works

Here's an overview of the client architecture:

```mermaid
graph LR
subgraph Author
    A[local.md]
    B(<strong>Converter</strong>)
    A-->B
    C[HTML]
    B-->C
    D("Git (hook)")
    C-- "git pre-push" -->D
    E(<strong>Build Script</strong>)
    D-->E
    E-->D
end
F(Server)
D-- "git post-push" -->F
```

* Converter: it's your responsibility to setup the converter, this part is transparent to our system, it does not care about how you setup the converter as long as it gets HTMLs as output.
  * For markdown, I recommend [Typora](https://typora.io/), you can also check [Github markdown conversion API](https://developer.github.com/v3/markdown/) if you like [GFM](https://en.wikipedia.org/wiki/Markdown#GitHub_Flavored_Markdown_(GFM))
  * For other formats, I recommend [Pandoc](https://pandoc.org/)

## Avoid Single Point Failure!

* Remember our design goal "Auto content backup"? You're responsible for auto backup as the admin
* Why do I need to backup my blog? Because you cannot afford to lose them and your PC may crash
* **But the server is keeping a copy of my blog, right? NO!** The server does nothing but deliver your blog, you cannot retrieve a copy from the server (for the sake of keeping the server minimal)! So the only copy is at your side, it could be a single point failure
* Well, use a 3rd party tool like `OneDrive`, `Dropbox`! They're reliable and are certainly better than a tool that I could write in every aspects

## Deployment

```shell
git clone git@gitlab.oit.duke.edu:zq29/zqauthor.git
cd zqauthor

git remote set-url --add --push origin ssh://git@<your host address>:2222/home/git/gitsrv/zqSite.git

# e.g
# git remote set-url --add --push origin ssh://git@qianzuncheng.com:2222/home/git/gitsrv/zqSite.git

```

