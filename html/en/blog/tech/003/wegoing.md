# An Auxiliary Program of "WeGoing" in WeChat's Mini Program

## Introduction

Mini programs are those programs running on the application "WeChat", which is widely used in China, and soon become popular after they're supported by WeChat. "WeGoing" is a game and also a member of mini programs, in which you control a rocket orbiting around stars and change orbit with tapping the screen.

When the rocket meets the intersection of two orbits, tap the screen and then the rocket will switch to the next orbit, and that's how you get marks. My program identifies the intersection and send the click signal to system when the rocket meets it.

## Demo

* Demo in GIF

  <img src="show.gif" width="500px">

* Highest Score: 23000

* How It Works

  <img src="show.jpg" width="500px">

## Implementation

The idea is simple: get screenshot -> identify orbits and rocket -> calculate intersection position -> send "click" signal to system when needed. However, there are many implementation details need to be concerned.

### Object Identification

Inspired by another auxiliary program, I decided to implement object identification using just RGB of a pixel without machine learning.

* Rocket Identification

  <img src="rocket_rec.jpg" width="800px">

  * Get a screenshot and find the RGB(constant) of a pixel on the head of this rocket

  * Traverse a screenshot every time you get one, and find the corresponding pixel that has the approximately same color as the one you got previously

  * And here comes a question: What if there's a pixel which is not on the head of the rocket but has the same color? Fortunately there's no other pixel that has the same color(i.e. the color of the pixel on rocket's head is unique)

  * The position of the pixel we find by previous steps are not accurate, which we'll discuss later

* Orbit Identification

  Idea: "randomly" find 3 points on the circle -> calculate the position of the center of the circle and the radius

  * Using the same technique we could find 3 points on a circle(orbit)

  * However it only works well in pure math when you randomly pick 3 points, because of the truncation error of a program. And here's how we pick the 3 points that will help us calculate the info we need more precisely

    <img src="3-points.jpg" width="400px">

    * We traverse pixels from bottom to the top, which is more efficient, and left to right

    * Every time we pick a point, just jump 10 pixels both upward and rightward. This helps: 1. avoiding picking the "same" pixel since lines do have width on screen; 2. eliminating truncation error

    * The picture above shows how we get 3 points we need

  * We could easily calculate the center and radius using simple math with 3 points

* The Intersection

Identifying directly the intersection point of two orbits can be difficult, for the lines has certain width, and one possible way is to calculate the intersection using pure math.

There's another way, a easier way, that is to determine whether the rocket is "on" the two orbits at the same time, and if so, it means the rocket has reached the intersection.

## Details and Error Elimination

### Which Two Orbits ?

There may be multiple orbits on the screen at the same time, but only two orbits are needed: the one the rocket is on, and the one right above it(this is why we traverse the screenshot from bottom to the top).

We denote the first orbit as orbit_1 and the one right above it as orbit_2. And here's the algorithm finding 2 orbits:

```
1. Find an orbit while traversing from bottom to the top
2. If the rocket is "in" the orbit, denote it as orbit_1, go to "1"
3. If orbit_1 has been found and the orbit is right above it denote it as orbit_2, go to "5"
4. Go to "1"
5. Exit
```

### What Causes Error ?

Life will become so much easier if everything can be pure math, and so is this question. Unfortunately we must face errors in real programming.

* The Inaccuracy of the Rocket's Position

  <img src="get_rocket_pos.jpg" width="500px">

  As has been mentioned above, there's error of the rocket's position because we just arbitrarily pick a point of the rocket. And as you can see in the picture above, the position identified by program(the small red square) is not completely "on" the circle. A little work needs to be done to optimize the position, and here's the result after optimization:

  <img src="get_rocket_pos2.jpg" width="500px">

  It's much more accurate than before!

  Still, the idea of this work is simple:

  <img src="get_rocket_pos3.jpg" width="500px">

  First we have a position "in" the orbit and we denote this position using black point. And we could calculate a straight line using this point and the center(shown in red point). Extend this line and we can get an intersection of it and the orbit.

  In programming, we can test pixel by pixel if they are actually "on" the circle to "extend" this line.

* Resolution Brings Error

  Suppose your screen has the resolution of 200x400 which means the neighborhood of a point can be displayed within 2x2. Then trouble comes if you have the screen with resolution 2000x4000 because the neighborhood of a point may need 20x20 to be displayed, and that's a disaster for our traversing algorithm since it's hard to figure out which pixel in the 20x20 area is at the most accurate position we wish to find. But bigger area means bigger tolerance for errors. We'll discuss that later.

### How to Reduce Error ? 

* Set a threshold: two values are considered equal when the absolute value of their difference is less than that threshold

  * The threshold is, of course, influenced by resolution

  * "Click" signal will be triggered too early when threshold is set too high

  * We tend to set the threshold to a reasonably big number since the rocket will stay on the current orbit forever if the threshold is too low

* The method used above to get a more accurate position of the rocket

## A Performance Defect

The program needs to get screenshot as frequent as possible because frequent sampling improves accuracy.

However the time consumed in getting a screenshot using Python PIL library exceeded far more than I expected at first. That's why I give up using Python and turn to C++.

PIL library's getting a screenshot(approximately 1000x1000 px, in my laptop, Intel I5, Win10) takes almost 0.2 seconds. You may consider it rather short but it's too long for this program. The following shows why 0.2 seconds is unacceptable:

<img src="screenshot_prob.jpg" width="500px">

The rocket is found to be at the right side of the intersection point at the time of t1 when we just get a screenshot, and to be at the left side of it at t2 when we take the next screenshot. The intersection is missed because the program is too busy at taking the 2nd screenshot in the 0.2 seconds.

To make sure the process of taking a screenshot is fast enough, I write a library using C++ and Windows API. It performs roughly 10 times faster than the PIL library.

Now let's recall the tolerance of error brought by big area. If the resolution is high, it means the probability of missing intersection point is lower within the same amount of time comparing with low resolution.

## A Non-monotonically Decreasing Function

Patience is required when reading this part due to my not so accurate expression and you can skip this part if you just want to get the general idea of this article.

<img src="decrease.jpg" width="500px">

Is the distance of the rocket and the center of the upper orbit always decreasing during the period when the rocket first start from t1 and then arrive at t2 and finally at the intersection point ? That is to say, is L2 always smaller than L1 ?

Why bother asking this ?

Recall the method we use when determining whether the rocket has arrived at the intersection: determine whether the rocket is on both orbit, and since the rocket is always on the oribit_1, we can simplify this condition to "whether the rocket is on the upper orbit", which is then mathematically equivalent to "if the distance of the rocket and the center of the upper circle is equal to radius".

Now move on to the programming from math. The condition above is equivalent to, if we denote the absolute value of the difference of the distance, of rocket and center, and radius as "d", the threshold as "t", "whether d is less than t". So "d" will be monotone decreasing if L2 is always smaller than L1, which then means that, if "d" is less than "t", the rocket is in the neighborhood of intersection point for sure.

When are we not sure whether the rocket is in that neighborhood when d is less than t? Recall what has been discussed of the threshold value set usually to a big value. If the threshold is, unfortunately, big enough and "d" is non-monotonically decreasing, then we're not sure. Because this always happens and it has brought lots of trouble to me when trying to get a higher score: when rocket is not at but rather close to intersection point, "d" becomes less than "t", but then "d" increases to the value higher than "t" and then when the rocket is at intersection, "d" becomes less than "t" for the second time.

Since it's impossible to know the future where there could probably be another chance that "d" is less than "t", the program has to send "click" signal the first time "d" is smaller than "t". Then you know what will happen to the unlucky little rocket.

The title of this part is a spoiler itself. It is proved non-monotonically decreasing by using counter-example I found.

### Solve Math Problem by Programming Trick

When the rocket failed to switch orbit at first, I didn't realize it was caused by the non-monotonically decreasing of "d" and tried to solve it by instinct.

Based on the observation that the rocket sometimes left the orbit too early an idea came to my mind that I should just postpone the time the rocket leaves.

Adding a constant of time, like 50ms, is clearly not a smart way. Let f(t) = the value of d at time t, then the image of f(t) will be something like this:

<img src="f_pos.jpg" width="500px">

The rocket should switch orbit at the time of t3, but why ?

At the time of t1, t2, t3, we get 3 consecutive screenshot. The best switching time is clearly somewhere between t2 and t3 but is impossible to catch in this case.

When at t1, the program basically "knows" nothing and at t2 it becomes aware that f(t) is decreasing without knowing if it's the minimum value which we wish to find. The process of first increasing and then decreasing of f(t) between t1 and t2 is what we discussed in the previous part, and it's usually a very short period of time based on my observation during which we should do nothing.

And the period between t2 and t3 is where the postponing happens. At t3, we first becomes aware that the rocket is leaving the intersection point so the rocket switches orbit immediately.

This strategy works better than I expected. By using a little trick postponing switching time, with a little luck, I solved this problem before even knowing the whole problem.

## Conclusion

Surprisingly, it works so well to identify objects using just color information(RGB) and this program is like a framework which can be modified to apply to other applications quickly.

Also surprisingly, a simple math problem need so much programming work. The whole process, through which so many difficulties are solved, is an enjoyable journey itself.

2018.01.31

Edited in English and uploaded on 2018.03.13
