# A Brief Introduction to DNN(Deep Neural Network)

> This article was written 1 year ago when I first came to know the word "Machine Learning". Now I'm taking a course named "Machine Learning and Data Mining" in this semester. Hopefully I'll find less mistakes that I've made in this article than I expected after finishing this course.

A paper I've been reading recently has discussed the feasibility of running ML(Machine Learning) algorithms on wearable devices. Though didn't fully understand it, I found DNN(Deep Neural Network) and CNN(Convolutional Neural Network) that have been mentioned on the paper rather interesting. So it took me about 3 days to do some research on DNN(and CNN) and finally wrote the code a simple DNN with the guidance on the Internet.

This article consists of 3 parts, the first part is my understanding of DNN without any formula or code. And the second is mainly about math and code. Finally some results of my research will be shown with pictures.

## What's DNN ?

DNN is like a new born baby at first. When you show him a picture of a puppy(this is input), you might see him pointing at the picture and tell you that's a biscuit(this is output). Then you say "No, that's a puppy"(this is target). The difference between a puppy and a biscuit is an error, which you want to eliminate. In order to do so, you constantly show the baby different pictures of a puppy and tell him what it is(this is the training process). And error decrease in the process of time until one day the baby(maybe a grown child) makes almost no mistake. That's a well trained DNN.

It's easy to tell people what DNN is, but it's hard to figure out why this approach works.

In the computer a DNN is nothing but tons of parameters and the relationship among them. With countless input(and target) those parameters are adjust by some certain algorithms to gain the ability to map input to the output we expected. Those parameters, I think, are something like experiences in our life, which work just fine but hardly no one knows the principle behind them. IT JUST WORKS.

### Framework of a DNN

![topology structure](./00.jpg)

The first layer on the left is input layer and right most is called output layer, and others are hidden layers. Every layer consists of many nodes which are connected as shown by the picture.(A Complete Bipartite Graph)

So where are the parameters? They are on the edge. Every edge has its own weight, which is the parameter mentioned above.

### The Procedure of Learning

Here's a picture I drew:

![01](./01.jpg)

The red line represents the inference process(forward) of the machine where input is mapped by functions in the node and weights on the edge to an output, from which the error is calculated.

Next, learning process(backward) is shown with the green line. Error is determined by weights and thus can be reduced by adjusting the values of them using mathematic tools like derivative.

The whole process is like taking a quiz. Students first answer questions based on what they already know and learn what they don't but are expected to know.

The following shows how these two procedures(forward and backward) can be implemented on a computer.

### Forward

![02](./02.jpg)

(i represents the number of a layer; j: the position of a node in a certain layer; k: the position of a node in the next layer; I: input; O: output)

Every node can be logically regarded as 2 parts: input and output, which are real numbers in most cases. The gray node is a constant and is important when it comes to math.

Here's how it works: every output in the node of the previous layer is multiplied by the corresponding weight(the edge connecting two nodes) and then summed up as input of a node in the current layer. And this node produces an output using an activation function with input.

You may be confused with what the initial values are in the nodes. Actually it doesn't matter what they are. They can be assigned or generated randomly because we keep adjusting them and they will converge to certain values in the end, and that's the power of learning.

### Backward

![03](./03.jpg)

By comparing the value of target and output we get the error, which is determined by weights(as has been mentioned above). So by calculating the derivative we could find a "direction" to reduce the error. It can be very difficult and time consuming to find the derivative of every weight. By using BP(Back Propagation) algorithm(chain rule) the backward process can be simplified.

## Math and Code

### Forward

This is quite simple. We add all the product of output on the previous layer and corresponding weight together and get:

$$
I_{i+1, k}=\Sigma^J_{j=1}O_{i, j}W_{i, j, k}
$$
Then we get the new output using activation function:

$$
O_{i+1, k}=\frac{1}{1+e^{-I_{i+1, k}}}
$$
The function is called Sigmoid and there are many other kinds of activation function.

Code is shown below(in Java):

``` java
    public double[][] layer;
    public double[][] layerErr;
    public double[][][] layer_weight;
    public double[][][] layer_weight_delta;
    public double mobp;
    public double rate;
```

```java
	public double[] computeOut(double[] in){
        for(int l = 1; l < layer.length; l++){
            for(int j = 0; j < layer[l].length; j++){
                double z = layer_weight[l - 1][layer[l - 1].length][j];
                for(int i = 0; i < layer[l-1].length; i++){
                    layer[l - 1][i] = l == 1 ? in[i] :layer[l - 1][i];
                    z += layer_weight[l - 1][i][j] * layer[l - 1][i];
                }
                layer[l][j] = 1/(1 + Math.exp(-z));
            }
        }
        return layer[layer.length-1];
    }	
```

### Backward

Here we use chain rule, gradient descent method, and BP algorithm.

The idea is simple: it is the same as a optimization problem using just derivative. Here's my script when calculating without hidden layer in the network:

![04](04.jpg)

The result is:

$$
\frac{\partial E}{\partial W_{i-1, j}}=(O_{i, j}-T_j)O_{i,j}(1-O_{i,j})O_{i-1},j
$$
Update weights:

$$
W_{i-1,j}=W_{i-1,j}-\eta \frac{\partial E}{\partial W_{i-1,j}}
$$
eta represents the rate of learning, which we will change manually.

## Results

### An Unexpected Result

I trained the network with the following data:

![05](./05.jpg)

The first group consists of 4 red points, and the second group has 4 green points. The circle I drew with red pen is the boundary between red and green points that most people may expect.

However, after 5000 times of training, I got the result as this:

![06](./06.jpg)

Black pixel means the network has the confidence over 70% to think this position belongs to the red group, while less than 30% confidence of thinking a position being red will make the pixel white. Gray pixel means the network is uncertain(confidence between 30% and 70%).

The result is not what I expected but it also cannot be regarded as a wrong answer. It makes much sense when you just draw the result with red and green:

![07](./07.jpg)

### A Perfect Result

Classical "xor" dataset:

![08](08.jpg)

With 5000 times of training I got the result:

![09](09.jpg)

The result is perfect due to the perfect dataset: the boundaries are linear, which can be represented easily with less parameters.

### Play with DNN

It soon came to my mind after I got the results above that if I could draw interesting patterns with random colors. So I made some change on the code and imported the Graphics2D library in Java.

When drawing a picture, the color(RGB) of a pixel in a certain position is determined by these rules: R is a function of the confidence whether the point belongs to the first group, and G is determined the same way but using the second group, and B is randomly generated. Then we got:

![10](./10.jpg)

Not so bad~

![11](./11.jpg)

2017.04.09

References:

1. How to implement DNN with 70 lines of Java code:

   <http://geek.csdn.net/news/detail/56086>

2. Lane N D, Bhattacharya S, Georgiev P, et al. An early resource characterization of deep learning on wearables, smartphones and internet-of-things devices[C]//Proceedings of the 2015 International Workshop on Internet of Things towards Applications. ACM, 2015: 7-12. 

3. DNN: Back Propagation

    <https://zhuanlan.zhihu.com/p/23270674>

4. What's CNN?

   <https://www.zhihu.com/question/52668301>

5. What's convolution?

   <https://www.zhihu.com/question/22298352/answer/35652690>

