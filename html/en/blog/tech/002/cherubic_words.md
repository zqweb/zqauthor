# Cherubic Words

A software developed by me to help memorize new English words.

Click here to download: <a href="/blog/tech/002/cwsetup.rar"></a>

## Introduction

Preparing GRE, feeling desperate when facing soooo many new words, I tried every possible way I could think of including this one.

The software I'm going to introduce shows new words again and again in a random or sequencial order, which is determined by users, and show them on top of all software to make sure that it can be seen on the screen all the time.

Based on the fact that I've got limited time to recite words on a book but the time of staring at computer screen is "infinite", I considered it a good way to recite words with this program.

So it took me one day in total to develop this program and another afternoon to test it and write docs.

## What the Program Can Do

* Stay on top of all programs showing new words

<img src="00.gif">

* Display words with different orders

   * by sequence

   * random

   * random without repetition

* Customize words that will be displayed

* Others

   * Window size

   * Window position

   * Interval of words (e.g. the 1st to the 1000th)

   * Interval of time of displaying one word

   * Color

## Usage

### Install

* Double click Cherubic Words Setup.exe to install

### Customize words

1. Find the directory where you installed this software

2. Find "bin"

   <img src="00.png">

3. Add/delete words in "words.txt" with format \<words\> \<meaning\>

   <img src="01.png">

### Other Settings

1. Find "config.txt" in directory "bin"

2. Change the attribute:

```c++
// the width and height of the window of which changing is not recommended
window_width = 800
window_height = 60

// the position of the window in the screen
x = 500
y = 500

/*
displaying strategy
0       random
1       random without repetition
2       by sequence, displaying within interval [begin, end)
*/
strategy = 1

// only works when displaying by sequence
begin = 0
end = 9999

// time in second during which one word will be displayed
interval = 5

// <word> color, <meaning> color
// using color in css, more info: http://www.w3school.com.cn/cssref/css_colornames.asp
word_color = #000066
trans_color = #303030
```

## Others

### A Bug

Sometimes the program could be coverd under other ones which is hard to fix since this is caused by the unstable performance of a 3rd party library(Qt: "WindowStaysOnTopHint" flag works randomly), here's some solution:

* Click the icon of this program in the taskbar for several times

* Reopen this program

* Close other programs that stays on top of it

### Development Environment

Win10 & VS2015 & Qt & C++

It is not guaranteed to work well in other platforms or environment :)