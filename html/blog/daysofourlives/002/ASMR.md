# 我的ASMR体验

## 前言

"在听到人们低声说话时，你有没有那种头皮发麻的感觉？"，我向几个人安利过ASMR，不过似乎没有人能理解，当他们听到像刷子刷过话筒的声音时，不会感觉很放松反而会起鸡皮疙瘩。

**按一种多数人能理解的方法来讲，如果你觉得听自然中的雨声、鸟叫声能感到放松的话，或许你正在体验一种低配版的ASMR。**

有ASMR体验的人还是少数，我决定将这种体验写下来，当有人问时直接给他这篇文章的链接。哦，顺便记录一下我这一年半的ASMR体验。文末会有一些YouTube的视频推荐。

## 什么是ASMR

> ASMR (Autonomous Sensory Meridian Response)，中文译名“自发性知觉经络反应”，是一个用于描述感知现象的新词，其特征是：对视觉、听觉、触觉、嗅觉或者感知上的刺激而使人在颅内、头皮、背部或身体其他范围内产生一种独特的、令人愉悦的刺激感。——百度百科

当然最直观的还是看一个视频：

B站：https://www.bilibili.com/video/av8639775/?from=search&seid=6340371087051488552

YouTube（需要科学地上网）：https://www.youtube.com/watch?v=P7kF9xzCRZ8&index=1&list=PLrqzSmX84WsuVsMtqgZvsS5HHX4iDDqb6

知乎上也有相关的讨论：https://www.zhihu.com/question/21948335

我对这种感觉最早的记忆是在小时候睡觉前，听大人在慢慢地轻声对话，感到一阵头皮发麻并且特别舒服，然后很快就睡着了。

### 感受

说了这么多，ASMR到底给人一种什么感觉呢？“独特的、令人愉悦的刺激感”实在是太抽象，然而我并不能找到更加合适的描述方法。就我而言，听到类似的声音时，我会感到内心十分的平静（并不想笑），偶尔会有头皮发麻的感觉，经常会有让人想缩成一团的感受。

如果无法亲身体验到，也许说再多也不能理解吧。哦，可悲的人。

### Trigger

一直不知道怎么翻译合适，大意就是“触发上述感受的声音/画面”。

百度百科列出了一些trigger：

>Guided Visualization（视觉引导）
>
>Motivational（激励）
>
>Nature（自然声音收集）
>
>Reading&Storytelling（读书&讲故事）
>
>Close-up（近耳）
>
>Salon,Spa&Barber（沙龙、Spa和剪头发）
>
>Medical,Exam&Health（医疗、考试和养生）
>
>Customer Service（客后服务）
>
>Face Painting（化妆）
>
>Fantasy & SciFi（假想的&科幻的）
>
>Brushing（刷）
>
>Chewing Sounds（咀嚼）
>
>Crinkling（发出沙沙声）
>
>Gum/Candy（吃口香糖）
>
>Hair Brushing（梳头发）
>
>Mouth Sounds（嘴发声音/湿唇）
>
>Page Flipping（翻书页）
>
>Rustling （发出瑟瑟声）
>
>Scratching（抓/刮）
>
>Tapping（触碰）
>
>Typing（打字）
>
>Water（水的声音）
>
>Light（灯光）
>
>Hands（手部动作）
>
>...

每个ASMR视频都会包含很多trigger来触发你的ASMR感受。

每个人的trigger都是不同的，比如我，比较喜欢听soft talking、咀嚼声、水声，但是像灯光这种trigger我一直无法理解，无感。

## 我这一年半的体验

### 初识ASMR

我是通过直播平台接触到ASMR的。16年暑假接触了直播，一直在斗鱼看WAR3的直播，有一天看到一条弹幕飘过“给你洗耳朵 进房间xxxxxx”，当时没太在意，后来这条弹幕刷的多了，我就特别好奇，洗什么耳朵？

没错，进去以后直播间就在播ASMR的视频，印象比较深刻的是一个韩国妹子吃虾的视频，吃了满满一大盘大虾，看的我都饿了，吃完虾又播了她吃薯片的视频，这两个视频看的我真是头皮发麻(in a good way)，感觉到了一种“独特的、令人愉悦的刺激感”。

### 正式入坑

#### Gibi

一直在直播平台看并不是很爽，因此只能按它播放的顺序看，于是就开始在视频网站上搜索。

坠吼的视频网站当然是YouTube啦，在YouTube上搜ASMR后，出现了满满的高质量视频，突然有种“终于找到组织”还有”今年粮食屯好“了的幸福感。

第一个接触到的做ASMR的YouTuber是Gibi，链接如下（需要科学地上网，下同）：

https://www.youtube.com/channel/UCE6acMV3m35znLcf0JGNn7Q

<img src="00.jpg" width="500px">

Gibi大概是周更吧，现在看的少了，主要是因为她作为一个native speaker语速太快了，而我的trigger是那种偏慢的语速，睡觉前我很少去看Gibi，偶尔睡醒后会去看看，就当是醒觉了哈哈哈哈，正好违背了听着入睡的初衷。

话说听这些whispers真的特别锻炼听力，可能这既是我托福不用练听力的原因吧（误）

后来在一个视频中听她说她还玩cosplay，于是就去关注了一下她的ins(https://www.instagram.com/gibiofficial/)，嗯感觉明明可以靠脸吃饭，却偏要做ASMR。放张图：

<img src="01.jpg" width="500px">

再后来，又在一个视频中听说她还打游戏，并且是twitch上的一个守望先锋游戏主播，给跪了orz。

#### Jellybean

Gibi催眠水平一般，却靠才华让人不舍取关。

https://www.youtube.com/channel/UCCjpp4TCjnRftS_LenXuSHw

而Jellybean真的是让人听着就想睡（这个表达似乎有些歧义）。最喜欢她做的No Talking系列，各种trigger是我认为做的最好的一个主播。

<img src="02.jpg" width="500px">

可惜中间有一段时间妹子的频道被黑她的人举报抄袭了，被YouTube封了一段时间。可是吃瓜群众看来真的没有什么抄袭啊，在解封后的一个视频了她也用了很多F-word表达了自己的愤怒。再后来更新就很少了，看了一下，最近的一次更新是在3个月前，真的很可惜。

### 成为日常

大三以来啊，状态不是很好。压力很大，各种失眠，一度以为自己要抑郁了。

而最快速有效的放松方式就是ASMR了，于是看ASMR便慢慢地成为了我的日常。

#### 最喜欢的ASMRtist：Latte

> ASMRtist: Blend of ASMR + artist，指做ASMR的人

无意间搜到了Latte，看了她上传的一部视频以后立刻就入坑了。先放链接：

https://www.youtube.com/channel/UCQe2Y7V-C9bNMAcCJCBvzQQ

<img src="03.jpg" width="500px">

（看到无名指上的戒指可以说是十分伤心了）

妹子会英语、韩语、日语，花了我很久才搞清楚，她是韩国人。

我当然只能看英语的了，由于她不是native speaker，因此说起英语来特别慢，这种soft slow talking完美符合我的trigger，而且她有些特别可爱的口头禅，不过用英语老师的话来说，这叫“句式太单一”，每部视频里，这些句式都是出镜率极高的：

>I'm going to be using ...
>
>Just like this.
>
>It feels like this.
>
>Good/Perfect

不仅trigger符合，而且每部视频的时间都很长，都在40分钟+，可以说，除了更新速度太慢以外，其它都很完美了！

文章开头B站的链接就是B站搬运工从YouTube上搬下来的Latte的视频。

#### ASMR Darling

闲逛时关注的主播，链接和图：

https://www.youtube.com/channel/UCikebqFWoT3QC9axUbXCPYw

<img src="04.jpg" width="500px">

按我的理解，她有着典型的美国妹子的性格，这也许就是为什么她有特别多的关注着而我却无法理解的原因吧。

评论区都叫她Taylor，我也这么叫吧。Taylor的语速也比较快，不过我最喜欢的一点就是她话多(also in a good way)，要是再慢一点就好了。

对她印象最深的是一个视频中Taylor突然冒出来一句：

> Mama Jesus, holy crap that is really satisfying

那是一个捏类似泥巴玩具的视频，这句话一出来真的是喷了。视频链接如下：

https://www.youtube.com/watch?v=edWU-TfHekg

大概在视频的28:10左右。

#### Pelagea

https://www.youtube.com/channel/UCNlMeUt5nOTQ-yfjXzRKVKA

<img src="05.jpg" width="500px">

肤白貌美大长腿，又是一个可以靠颜值吃饭的orz。德国妹子，可惜trigger做的并不吸引我，关注她是因为实在太美了(0ω0)

#### FrivolousFox

https://www.youtube.com/channel/UCoNfsDH8sZe13u7rSxaEBkw

<img src="06.jpg" width="500px">

左手上有个十字架文身，原谅我无知我不知道这有什么含义，给人一种很社会的感觉。

这个主播的特点是会！唱！歌！ASMR低声唱那种，哇有一次唱Six Feet Under真的是让人感觉才华要溢出了。她还有一次和Gibi合唱的视频。

评论区有人说：小时候听lullaby入睡，长大了来这里听ASMR歌曲入睡

#### 中国主播Tingting

https://www.youtube.com/channel/UClqNSqnWeOOUVkzcJFj4rBw

<img src="07.jpg" width="500px">

国内的ASMR，恕我直言，真的是发展歪了。去B站搜ASMR，排名靠前的全是胸和腿。

我只是想睡觉，想听到各种trigger，可现在没有办法，有才华的人都在YouTube这个404网站上。

听说pornhub有ASMR板块？（搓搓手）

跑题了，Tingting ASMR做的确实不错，一口正宗的Chinglish听着倍感亲切哈哈哈哈，最近关注者似乎越来越多了，资瓷！

## ASMRtist推荐

（详细介绍见上文咯，以下网址访问需要科学地上网）

* **Latte**（强烈推荐！）

  https://www.youtube.com/channel/UCQe2Y7V-C9bNMAcCJCBvzQQ

* **Gibi**

  https://www.youtube.com/channel/UCE6acMV3m35znLcf0JGNn7Q

* **Tingting**

  https://www.youtube.com/channel/UClqNSqnWeOOUVkzcJFj4rBw

* **ASMR Darling**

  https://www.youtube.com/channel/UCikebqFWoT3QC9axUbXCPYw

* **FrivolousFox**

  https://www.youtube.com/channel/UCoNfsDH8sZe13u7rSxaEBkw

* **Pelagea**

  https://www.youtube.com/channel/UCNlMeUt5nOTQ-yfjXzRKVKA

* **Jellybean**

  https://www.youtube.com/channel/UCCjpp4TCjnRftS_LenXuSHw

## 结语

ASMR近些年才火起来，本老年人终于跟上了一波潮流。

主流媒体近两年也开始做一些相关的内容，《W》杂志曾经邀请过Gal Gadot和Margot Robbie去做了几分钟的ASMR视频，宜家也做了一个20多分钟的ASMR的家居广告。越来越多的人开始接受ASMR。

可是我还没有找到和我一样喜欢ASMR的线下的朋友，大概这还是一个小众的爱好。

心情不好时，睡不着觉时，甚至无聊时，我都会去看一些ASMR的视频，这已经成为我生活中不可或缺的一部分。

伦敦也开了线下ASMR体验店，希望国内也早点开出第一家体验店。

2018.02.02

