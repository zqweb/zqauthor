# 研二上学期总结

> placeholder

### 1. 学习

学习打头吧，谁让这是学期总结呢

这学期一共三门课，`CS 510 OPERATING SYSTEMS`, `ECE 555 PROBABILITY ELEC AND COMP EGRS`, `ECE 586 VECTOR SPACE METHODS APPL`

第一门操作系统课还是蛮简单的，复习了一下本科的内容，也学到了不少新东西，主要是关于系统安全的；很多时候讲的还不如本科操作系统深入

第二门课主要是工程领域的概率论，和reliability相关，讲了不少Markov chain相关的内容，蛮有意思，上课学习造火箭，考试考拧螺丝

第三门课值得好好学，大概是graduate level的数学基础课吧，学到了metric space，vector space等等，是对以往学过的数学的一个抽象，在这门课中数学和计算机的关系更加紧密了

本学期还是Zoom大学，所有课都是线上完成，前几个星期还有兴趣跟着课一起上，后来直接asynchronized学习：不上课然后课后二倍速看录像自学

每天一个DDL，任务量很小，大概每天4小时的工作量就能搞定，有大量的时间…养生

下图就是我循环了十几周的日程：

<img src="schedule.jpg" width="800px">

<br>

### 2. 

2020-08-25 23:26 于Durham