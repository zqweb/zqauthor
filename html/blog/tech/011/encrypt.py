# ZQ File Encryption Protocol, BYTE version
# qianzch@gmail.com
# 05/12/2021

doc = '''
Before running this, you should: 
* Make sure './zqfe' is empty IF it already exists
* Or there's nothing important in that directory

This script will:
* Ask you to set a password for later encryption use
* Encrypt every regular file within the current directory
*   and save the output files(with suffix '.zqfe')in the 
*   'zqfe' directory, which will be created if not exist
* Rename every .zqfe file in the zqfe directory to a random name
* Save the mapping info of names in './zqfe/namemapping.dict'

Please make sure you:
* REMEMBER THE PASSWORD YOU SET
*   (or you may lose all your files in the end)
* SAVE THE 'namemapping.dict' FILE
*   (or you leave all your files with weird names)
'''

import os
import time
import pickle
import random
import numpy as np

# for better read performance
class ReadFileCache():
    def __init__(self, filename, cacheSizeMB = 16):
        self.fr = open(filename, 'rb')
        self.cacheSizeMB = cacheSizeMB
        self.buffer = bytearray()
        self.ptr = 0 # buffer index
    def __read(self):
        rest = self.buffer[self.ptr: ] # unconsumed
        next = self.fr.read(self.cacheSizeMB * 1024 * 1024) # more
        self.buffer = rest + next
        self.ptr = 0 # reset ptr
    def read(self, nbytes):
        # if used up buffer content, need to read more
        if self.ptr + nbytes > len(self.buffer):
            self.__read()
        prevPtr = self.ptr
        # how many will we give?
        self.ptr = len(self.buffer) if (self.ptr + nbytes > len(self.buffer)) else self.ptr + nbytes
        # give the most we could
        return self.buffer[prevPtr: prevPtr + nbytes]
    def close(self):
        self.fr.close()

# for better write performance
class WriteFileCache():
    def __init__(self, filename, cacheSizeMB = 16):
        self.fw = open(filename, 'wb')
        self.cacheSizeBytes = cacheSizeMB * 1024 * 1024
        self.buffer = bytearray()
        self.currSize = 0
        self.timer = time.time()
    def write(self, content: bytearray):
        self.buffer.extend(content)
        self.currSize += len(content)
        if self.currSize > self.cacheSizeBytes:
            self.flush()
    def flush(self):
        self.fw.write(self.buffer)
        self.fw.flush()
        self.buffer = bytearray()
        self.currSize = 0
        #self.reportThroughput()
    def reportThroughput(self):
        deltaTime = time.time() - self.timer
        self.timer = time.time()
        print("Throughput: {:.2f} MB/s".format(self.cacheSizeBytes / 1024 / 1024 / deltaTime))
    def close(self):
        self.flush()
        self.fw.close()

class ZQFileDencrypter():
    def __init__(self, inputFilename, outputFilename, bpasswd: bytes):
        self.fr = ReadFileCache(inputFilename)
        self.fw = WriteFileCache(outputFilename)
        # optimization: increase the length of our password to roughly KB level and also strided
        self.bpasswd = bytearray(bpasswd + bytes(b'\0') * len(bpasswd)) * (128 * 1024 // (2 * len(bpasswd)))
    def dencrypt(self):
        stride = len(self.bpasswd)
        while True:
            content = self.fr.read(stride)
            if not content: break # EOF
            output = byteStrAdd(content, self.bpasswd[: len(content)])
            self.fw.write(output)
        self.fw.flush()
    def close(self):
        self.fr.close()
        self.fw.close()

# convert user input password to bytes
def passwdToByteStr(passwd: str) -> bytes:
    bpasswd = bytes(passwd, 'utf-8')
    if all([not e for e in bpasswd]):
        raise Exception('password all zero, please choose another password')
    return bpasswd

# XOR
def byteStrAdd(content: bytearray, passwd: bytearray) -> bytes:
    assert len(content) == len(passwd), 'byte strings must be of the same length'
    return np.bitwise_xor(content, passwd).tobytes()

# filename: input filename
# dir: output dir
# show: if print log on screen
def getOutputFilename(filename: str, dir = '', show: bool = True) -> str:
    # test if file exists
    try:
        f = open(filename, 'rb')
    except IOError:
        if show: print('file {} does not exist or unable to read'.format(filename))
        return None
    # get output name
    filename = os.path.basename(filename)
    if filename[-5: ] == '.zqfe':
        if show: print('decrypting file {}'.format((filename)))
        return '{}{}{}'.format(dir, filename[: -5], '.ori')
    else:
        if show: print('encrypting file {}'.format(filename))
        return '{}{}{}'.format(dir, filename, '.zqfe')



# * map any filenames to {prefix}{generated [0-9|a-z|A-Z] sequence}{suffix}
# * to prevent name collision, only call this function once for a directory
# * save the mapping info into <mappingFile>
def encryptFilenames(filenameList, prefix = 'data_', suffix = '.data', mappingFile = './zqfe/namemapping.dict'):
    nFiles = len(filenameList)
    
    # the sequence is at least 8 char or, if there are more files, nFiles // 62 + 3 chars
    #
    # we could represent 62 files per char
    # +3 means at least, we could choose 1 out of 62*(3-1) from the generated names
    nChars = max(8, nFiles // 62 + 3)
    charSet = [chr(ord('0') + i) for i in range(10)]
    charSet += [chr(ord('a') + i) for i in range(26)]
    charSet += [chr(ord('A') + i) for i in range(26)]
    
    newNames = set()
    # we find every file a new name
    while len(newNames) != nFiles:
        newNames.add(''.join(random.sample(charSet, nChars)))
    
    namemapping = {}
    for oldPath, charSeq in zip(filenameList, newNames):
        newName = '{}{}{}'.format(prefix, charSeq, suffix)
        newPath = os.path.join(os.path.dirname(oldPath), newName)
        print('----- Rename {} to {}'.format(oldPath, newPath))
        os.rename(oldPath, newPath)
        namemapping[newName] = os.path.basename(oldPath)
    
    # save this mapping at last is dangerous, but anyway...
    with open(mappingFile, 'wb') as fw:
        pickle.dump(namemapping, fw)



if __name__ == '__main__':
    print(doc)
    yesno = input('continue? y/n')
    if yesno != 'y': exit()

    bpasswd = passwdToByteStr(input('type your password:'))
    if not os.path.exists('./zqfe'):
        os.makedirs('./zqfe')
    
    for file in [f for f in os.listdir('./') if os.path.isfile(f)]:
        # skip this file
        if file == os.path.basename(__file__): continue

        print('----- Going to process {}, y/n? y'.format(file))

        # ask for permission
        # yesno = input()
        # if yesno != 'y': continue

        # do it!
        outputFilename = getOutputFilename(file, './zqfe/')
        print('----- Output {}'.format(outputFilename))
        fileDencrypter = ZQFileDencrypter(file, outputFilename, bpasswd)
        fileDencrypter.dencrypt()
        fileDencrypter.close()
        print('----- Done!')

    encryptFilenames([os.path.join('./zqfe/', f) for f in os.listdir('./zqfe/') if f.endswith('.zqfe')])