# ZQ File Encryption Protocol, BYTE version
# qianzch@gmail.com
# 05/12/2021

doc = '''
Before running this, you should: 
* Make sure './ori' is empty IF it already exists
* Or there's nothing important in that directory
* Make sure 'namemapping.dict' is in the current directory

This script will:
* Rename every file to its original name if 'namemapping.dict' is 
*   in the current directory
* Ask you the password you used with encryption
*   (it has no idea if it's correct, if not, you'll get weird output)
* Decrypt every regular file within the current directory
*   and save the output files in the 'ori' directory you just created
'''

import os
import time
import pickle
import numpy as np

# for better read performance
class ReadFileCache():
    def __init__(self, filename, cacheSizeMB = 16):
        self.fr = open(filename, 'rb')
        self.cacheSizeMB = cacheSizeMB
        self.buffer = bytearray()
        self.ptr = 0 # buffer index
    def __read(self):
        rest = self.buffer[self.ptr: ] # unconsumed
        next = self.fr.read(self.cacheSizeMB * 1024 * 1024) # more
        self.buffer = rest + next
        self.ptr = 0 # reset ptr
    def read(self, nbytes):
        # if used up buffer content, need to read more
        if self.ptr + nbytes > len(self.buffer):
            self.__read()
        prevPtr = self.ptr
        # how many will we give?
        self.ptr = len(self.buffer) if (self.ptr + nbytes > len(self.buffer)) else self.ptr + nbytes
        # give the most we could
        return self.buffer[prevPtr: prevPtr + nbytes]
    def close(self):
        self.fr.close()

# for better write performance
class WriteFileCache():
    def __init__(self, filename, cacheSizeMB = 16):
        self.fw = open(filename, 'wb')
        self.cacheSizeBytes = cacheSizeMB * 1024 * 1024
        self.buffer = bytearray()
        self.currSize = 0
        self.timer = time.time()
    def write(self, content: bytearray):
        self.buffer.extend(content)
        self.currSize += len(content)
        if self.currSize > self.cacheSizeBytes:
            self.flush()
    def flush(self):
        self.fw.write(self.buffer)
        self.fw.flush()
        self.buffer = bytearray()
        self.currSize = 0
        #self.reportThroughput()
    def reportThroughput(self):
        deltaTime = time.time() - self.timer
        self.timer = time.time()
        print("Throughput: {:.2f} MB/s".format(self.cacheSizeBytes / 1024 / 1024 / deltaTime))
    def close(self):
        self.flush()
        self.fw.close()

class ZQFileDencrypter():
    def __init__(self, inputFilename, outputFilename, bpasswd: bytes):
        self.fr = ReadFileCache(inputFilename)
        self.fw = WriteFileCache(outputFilename)
        # optimization: increase the length of our password to roughly KB level and also strided
        self.bpasswd = bytearray(bpasswd + bytes(b'\0') * len(bpasswd)) * (128 * 1024 // (2 * len(bpasswd)))
    def dencrypt(self):
        stride = len(self.bpasswd)
        while True:
            content = self.fr.read(stride)
            if not content: break # EOF
            output = byteStrAdd(content, self.bpasswd[: len(content)])
            self.fw.write(output)
        self.fw.flush()
    def close(self):
        self.fr.close()
        self.fw.close()


def passwdToByteStr(passwd: str) -> bytes:
    bpasswd = bytes(passwd, 'utf-8')
    if all([not e for e in bpasswd]):
        raise Exception('password all zero, please choose another password')
    return bpasswd

def byteStrAdd(content: bytearray, passwd: bytearray) -> bytes:
    assert len(content) == len(passwd), 'byte strings must be of the same length'
    return np.bitwise_xor(content, passwd).tobytes()

def getOutputFilename(filename: str, dir = '', ori_suffix = '.ori', show: bool = True) -> str:
    # test if file exists
    try:
        f = open(filename, 'rb')
    except IOError:
        if show: print('file {} does not exist or unable to read'.format(filename))
        return None
    # get output name
    filename = os.path.basename(filename)
    if filename[-5: ] == '.zqfe':
        if show: print('decrypting file {}'.format((filename)))
        return '{}{}{}'.format(dir, filename[: -5], ori_suffix)
    else:
        if show: print('encrypting file {}'.format(filename))
        return '{}{}{}'.format(dir, filename, '.zqfe')



def decryptFilenames(mappingFile, dir = './'):
    try:
        with open(mappingFile, 'rb') as fr:
            nameMapping = pickle.load(fr)
    except:
        print('Did not found name mapping file {}!'.format(mappingFile))
        return
    
    for k, v in nameMapping.items():
        oldPath = os.path.join(dir, k)
        newPath = os.path.join(dir, v)
        print('----- Rename {} to {}'.format(oldPath, newPath))
        try:
            os.rename(oldPath, newPath)
        except:
            print('Cannot find {}'.format(oldPath))
            continue



if __name__ == '__main__':
    print(doc)
    yesno = input('continue? y/n')
    if yesno != 'y': exit()

    decryptFilenames('namemapping.dict', './')

    bpasswd = passwdToByteStr(input('type your password:'))
    if not os.path.exists('./ori'):
        os.makedirs('./ori')
    
    for file in [f for f in os.listdir('./') if os.path.isfile(f)]:
        # skip this file
        if file == os.path.basename(__file__): continue
        if file == 'namemapping.dict': continue

        print('----- Going to process {}, y/n? y'.format(file))

        # ask for permission
        # yesno = input()
        # if yesno != 'y': continue

        # do it!
        outputFilename = getOutputFilename(file, './ori/', '')
        print('----- Output {}'.format(outputFilename))
        fileDencrypter = ZQFileDencrypter(file, outputFilename, bpasswd)
        fileDencrypter.dencrypt()
        fileDencrypter.close()
        print('----- Done!')