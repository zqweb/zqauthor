# 部署指南

## 文件组织说明

```
|-- src
|    |-- data
|    |    |-- simple_train.csv
|    |-- model
|    |    |-- DNN
|    |    |-- DNN_simple
|    |    |-- tensorboard
|    |    |-- tensorboard_simple
|    |-- module
|    |    |-- bg_data.py
|    |    |-- improver.py
|    |    |-- pre_proc.py
|    |-- result
|    |    |-- delta_data.csv
|    |    |-- evaluate_labels.csv
|    |    |-- improved_data.csv
|    |-- main.py
|    |-- README(部署指南).pdf
|-- 15336144-钱尊诚-基于机器学习的过程改进.pdf
```

* `src/data`目录下存放训练数据，需要命名为`train.csv`或`simple_train.csv`，分别对应正常运行和小数据集上验证时的运行数据，默认采用后者，为了节省空间，该文件夹内只放了原数据集的一个小子集`simple_train.csv`
* `src/model`目录下存放训练好的模型，为了节省空间，**内容已删除**，可以通过在本机上运行程序再次获得
* `src/module`目录下存放程序的各个模块
  * `bg_data.py`主要是数据的各种变换以及IO操作
  * `improver.py`实现了整个模型
  * `pre_proc.py`数据预处理
* `src/result`目录下存放了各种结果
  * `delta_data.csv`记录了改进数据后的变化情况
  * `evaluate_labels.csv`记录了分类器输出的标签，debug用
  * `improved_data.csv`记录了数据提升后的最终结果（有降维、归一化处理）
* `src/main.py`用来运行模型
* `src/15336144-钱尊诚-基于机器学习的过程改进.pdf`实验报告

## 部署环境

* Windows10
* Python3.6
  * tensorflow
  * sklearn
  * numpy
  * pandas

## 一次最简单的完整的运行过程

接下来是一个完成的运行过程的例子，当然还有其它很多参数可以控制，详情见`main.py`中的源代码。

### 数据预处理

在`src/`目录下执行：

```
python main.py --mode=1
```
此时生成了`src/data/simple_train_proced.csv`文件

###训练模型

在`src/`目录下执行：

```
python main.py --mode=2
```

此时会开始训练模型：

<img src="rm00.png" width="600px">

训练完毕后，目录`src/model/DNN_simple`和`src/model/tensorboard_simple`中会出现对应的内容。

在`src/model/tensorboard_simple/`下执行：

```
tensorboard --logdir=. --host=127.0.0.1
```

然后打开浏览器进入`127.0.0.1/6006`就能看到tensorboard：

<img src="rm01.png" width="600px">



### 检查分类器在训练集上的准确率

在`src/`目录下执行：

```
python main.py --mode=4
```

### 获得结果

在`src/`目录下执行：

```
python main.py --mode=3
```

<img src="rm02.png" width="600px">

此时`src/result/`中会出现3个`.csv`文件代表结果。