#include <iostream>
#include <fstream>
#include <ctime>
//#include <Windows.h>
using namespace std;

#define PI_FILE "Pi.txt"

void read_pi(char **pi, int &len);
void get_pattern(char **pattern, int &len);
void get_next_array(char *p, int *next, int len);
int naive_search(char *s, int s_len, char *p, int p_len);
int KMP_search(char *s, int s_len, char *p, int p_len, int *next);

int main () {
	
	char *pi;
	int pi_len;
	read_pi(&pi, pi_len);
	
	while(true) {
		
		// get pattern, e.g. your birthday "19990101"
		char *pattern;
		int pattern_len;
		get_pattern(&pattern, pattern_len);
		
		clock_t start, end;
		// KMP
		//start = clock();
		int next[pattern_len];
		get_next_array(pattern, next, pattern_len);
		int pos = KMP_search(pi, pi_len, pattern, pattern_len, next);
		//end = clock();
		//cout << "KMP search time " << 1.0 * (end - start) / CLOCKS_PER_SEC << "s" << endl;

		/* 
		// naive
		start = clock();
		cout << naive_search(pi, pi_len, pattern, pattern_len) << endl;
		end = clock();
		cout << "Naive search time " << 1.0 * (end - start) / CLOCKS_PER_SEC << "s" << endl;
		*/
		
		if(pos != -1)
			cout << "Position after dot(begin with 1): " << pos - 1 << endl << endl;
		else
			cout << "Not exists !" << endl << endl;
		
		delete[] pattern;
		
	}
	
	delete[] pi;
	return 0;
}

void read_pi(char **pi, int &len) {
	// clock_t start = clock();
	ifstream is(PI_FILE, ifstream::binary);
	
	is.seekg(0, is.end);
	len = is.tellg();
	is.seekg(0, is.beg);
	
	*pi = new char[len];

	is.read(*pi, len);
	is.close();
}

void get_pattern(char **pattern, int &len) {
	string s;
	cout << "Please input pattern: ";
	cin >> s;
	len = s.length();
	*pattern = new char[len + 1];
	for(int i = 0; i < len; i++)
		(*pattern)[i] = s[i];
	(*pattern)[len] = '\0';
}

void get_next_array(char *p, int *next, int len) {
	int p_len = len;
	next[0] = -1;
	int k = -1;
	int j = 0;
	while (j < p_len - 1) {
		if (k == -1 || p[j] == p[k]) {
			++j;
			++k;
			if (p[j] != p[k]) next[j] = k;
			else next[j] = next[k];
		} else {
			k = next[k];
		}
	}
}

int naive_search(char *s, int s_len, char *p, int p_len) {
	int i = -1, j = 0;
	while(i < s_len) {
		while(j < p_len) {
			i++;
			if(s[i] == p[j]) j++;
			else break;
			if(j == p_len) return i - j + 1;
		}
		j = 0;
	}
	return -1;
}

int KMP_search(
	char *s, int s_len,
	char *p, int p_len,
	int *next
	) {

	int i = 0;
	int j = 0;
	while (i < s_len && j < p_len) {
		if (j == -1 || s[i] == p[j]) {
			i++;
			j++;
		} else {   
			j = next[j];
		}
	}
	if (j == p_len) return i - j;
	else return -1;
}
