# ZQ File Encryption Protocol, BYTE version
# qianzch@gmail.com
# 05/12/2021

def passwdToByteStr(passwd: str) -> bytes:
    bpasswd = bytes(passwd, 'utf-8')
    if all([not e for e in bpasswd]):
        raise Exception('password all zero, please choose another password')
    return bpasswd

def byteStrAdd(content: bytes, passwd: bytes) -> bytes:
    assert len(content) == len(passwd), 'byte strings must be of the same length'
    return bytes([a ^ b for a, b in zip(content, passwd)])

# won't modify the original file
# encrypt is the same as decrypt
def dencryptFile(filename: str, bpasswd: bytes, outputFilename: str):
    stride = len(bpasswd)
    with open(filename, 'rb') as fr: # read
        with open(outputFilename, 'wb') as fw: # write
            while True:
                content = fr.read(stride)
                if not content: return # EOF
                output = byteStrAdd(content, bpasswd[: len(content)])
                fw.write(output)
                # stride
                content = fr.read(stride)
                fw.write(content)

def getOutputFilename(filename: str, show: bool = True) -> str:
    try:
        f = open(filename, 'rb')
    except IOError:
        if show: print('file {} does not exist or unable to read'.format(filename))
        return None
    if filename[-5: ] == '.zqfe':
        if show: print('decrypting file {}'.format((filename)))
        return filename[: -5] + '.ori'
    else:
        if show: print('encrypting file {}'.format(filename))
        return filename + '.zqfe'

if __name__ == '__main__':
    filename = input('type your filename:')
    outputFilename = getOutputFilename(filename)
    passwd = input('type your password:')
    dencryptFile(filename, passwdToByteStr(passwd), outputFilename)