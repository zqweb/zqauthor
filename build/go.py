import os, os.path, sys
import re

CN_ROOT = '../html/blog/'
EN_ROOT = '../html/en/blog/'
CN_DIRS = ['daysofourlives', 'kraken', 'tech']
EN_DIRS = ['tech']

# check if dirs under a root has illegal dir names
# legal format \d\d\d
def checkDirNames(root, dirs):
	for d in [root + e for e in dirs]:
		if not os.path.isdir(d):
			raise Exception("Dir <{}> not exists!".format(d))
		for e in os.listdir(d):
			if not re.match('\d\d\d', e):
				raise Exception("Dir <{}> not recognized!".format(d + e))

def main():
	checkDirNames(CN_ROOT, CN_DIRS)
	checkDirNames(EN_ROOT, EN_DIRS)
	''''
	for d in DIRS:
		for e in os.listdir(HTML_ROOTS[0] + d):
			if not re.match('\d\d\d', e):
				print('Directo')
	'''


if __name__ == '__main__':
	main()
